var app = angular.module('app', ['luegg.directives']);

app.controller('MainCtrl', ['$scope', '$http', function ($scope, $http) {
    String.prototype.splice = function (idx, rem, s) {
        return (this.slice(0, idx) + s + this.slice(idx + Math.abs(rem)));
    };
    $scope.chats = [];
    $scope.test = 'hey';
    $scope.user = {};
    $scope.chat = [];
    $scope.chats = [];
    $scope.newChats = [];
    $scope.$watch('chats', function () {
        if ($scope.chats) {
            console.log($scope.chats);
            $scope.chats.forEach(function (i) {
                var data = {
                    id: i.id
                }
                console.log(data);
                $http.post('http://127.0.0.1:3000/api/findUsernames', data)
                    .success(function (data) {
                        console.log(data);
                        $scope.newChats.push(data);
                    })
                    .error(function (err) {
                        return console.log(err);
                    });
            });
        }
    });
    function checkURL(url) {
        return (url.match(/\.(jpeg|jpg|gif|png)$/) != null);
    }
    $scope.$watch('chat', function () {
        if ($scope.chat.messages) {
            $scope.chat.messages.forEach(function (i) {
                console.log('Checking...')
                var url = checkURL(i.message);
                if (url === true) {
                    i.src = i.message;
                }
                console.log(url);
            });
        }
    });
    var socket = io();
    $scope.isChat = false;
    $scope.chatClicked = function (chat) {
        $scope.isChat = true;
        $scope.chat.messages = [];
        var data = {
            id: chat.id
        }
        $scope.id = chat.id;
        $http.post('http://127.0.0.1:3000/api/findChat', data)
            .success(function (data) {
                console.log(data);
                $scope.chat = data;
            });
        socket.emit('subscribe', chat.id);
    }
    $scope.addChat = function (newChat) {
        $scope.username = newChat.username;
        var data = {
            username: newChat.username
        };
        $http.post('http://127.0.0.1:3000/api/addChat', data)
            .success(function (data) {
                console.log(data);
                $scope.response = data;
            });
    }

    $scope.sendChat = function (sent) {
        socket.emit('send message', {
            id: $scope.id,
            message: sent.message,
            user: $scope.user.username
        });
        var data = {
            id: $scope.id,
            message: sent.message
        }
        $http.post('http://127.0.0.1:3000/api/sendChat', data)
            .success(function (data) {
                console.log(data);
            });
        sent.message = '';
    }

    socket.on('conversation private post', function (data) {
        console.log(data);
        var url = checkURL(data.message);
        if (url === true) {
            data.src = data.message;
        }
        $scope.$apply(function () {
            $scope.chat.messages.push(data);
        });
        console.log($scope.chat.messages);
    });

    /*$scope.sendImage = function (sent) {
        var data = {
            url: 'http://25.media.tumblr.com/tumblr_md1yfw1Dcz1rsx19no1_1280.png'
        }
        console.log(data);
        $http.post('http://127.0.0.1:3000/api/sendImage', data)
            .success(function (data) {
                console.log(data);
                sent.message = data.link;
            });
    }*/
}]);