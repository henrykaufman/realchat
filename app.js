var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var passport = require('passport');
var session = require('express-session');
var TwitterStrategy = require('passport-twitter').Strategy;
var users = {};

var app = express();
var socket_io = require("socket.io");

// Express
var app = express();

// Socket.io
var io = socket_io();
app.io = io;


var usersDB = mongoose.createConnection('mongodb://127.0.0.1:27017/Chat-Users-v1');
var convoDB = mongoose.createConnection('mongodb://127.0.0.1:27017/Chat-Convos-v1')

usersDB.on('open', function () {
    console.log('connected to users');
});

convoDB.on('open', function () {
    console.log('connected to convo');
});

var UserSchema = new mongoose.Schema({
    name: String,
    username: String,
    chats: [{
        id: String
    }]
});

var ConvoSchema = new mongoose.Schema({
    users: [String],
    messages: [{
        user: String,
        message: String,
        timestamp: {
            type: Date,
            default: Date()
        }
    }]
});

var User = usersDB.model('User', UserSchema);
var Convo = convoDB.model('Convo', ConvoSchema);

passport.use(new TwitterStrategy({
        consumerKey: 'MG6oncNmbgtGNochaACHDV4d3',
        consumerSecret: 'u2KuBIOpYggVXWhFEVI447TxpLX6ZHJVeYnRfmQZDd9sE2hz4H',
        callbackURL: "http://127.0.0.1:3000/auth/twitter/callback"
    },
    function (token, tokenSecret, profile, done) {
        User.findOne({
            twitterId: profile.id
        }, function (err, user) {
            if (err) return console.log(err);
            if (user) {
                console.log(user);
                return done(null, user);
            } else {
                console.log('No User');
                console.log(profile);
                var NewUser = User({
                    twitterId: profile.id,
                    username: profile.username,
                    name: profile.displayName,
                    chats: []
                });
                NewUser.save(function (err, NewUser) {
                    if (err) return console.log(err);
                    console.log(NewUser);
                    return done(null, NewUser);
                });
            }
        })
    }
));

passport.serializeUser(function (user, done) {
    done(null, user);
});

passport.deserializeUser(function (id, done) {
    User.findById(id, function (err, user) {
        done(err, user);
    });
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(session({
    secret: 'keyboard cat'
}));
app.use(passport.initialize());
app.use(passport.session());

app.get('/', function (req, res) {
    if (req.user) {
        User.findOne({
            username: req.user.username
        }, function (err, chats) {
            if (err) return console.log(err);
            console.log(chats);
            if (chats) {
                res.render('index', {
                    user: req.user,
                    chats: chats.chats
                });
            } else {
                res.render('index', {
                    user: req.user
                });
            }
        });
    } else {
        res.render('index');
    }
});

app.post('/api/findUsernames', function (req, res) {
    var id = req.body.id;
    console.log(id);
    Convo.findOne({
        _id: id
    }, function (err, obj) {
        if (err) return console.log(err);

        function removeA(arr) {
            var what, a = arguments,
                L = a.length,
                ax;
            while (L > 1 && arr.length) {
                what = a[--L];
                while ((ax = arr.indexOf(what)) !== -1) {
                    arr.splice(ax, 1);
                }
            }
            return arr;
        }

        var removed = removeA(obj.users, req.user.username);
        var pushed = {
            id: obj._id,
            user: removed[0]
        }
        console.log(pushed);
        res.send(pushed);
    });
});

app.post('/api/addChat', function (req, res) {
    var username = req.body.username;
    console.log(username);
    var NewConvo = Convo({
        users: [username, req.user.username]
    });
    NewConvo.save(function (err, NewConvo) {
        if (err) return console.log(err);
        console.log(NewConvo);
        User.findOneAndUpdate({
            username: username
        }, {
            $push: {
                chats: {
                    id: NewConvo._id
                }
            }
        }, {
            safe: true,
            upsert: true
        }, function (err, user) {
            if (user) {
                console.log('step 1');
                if (err) return console.log(err);
                console.log(user);
                console.log(req.user.username);
                User.findOneAndUpdate({
                    username: req.user.username
                }, {
                    $push: {
                        chats: {
                            id: NewConvo._id
                        }
                    }
                }, {
                    safe: true,
                    upsert: true
                }, function (err, user) {
                    console.log('step 2');
                    if (err) return console.log(err);
                    console.log(user);
                    res.send('Chat made!');
                });
            } else {
                Convo.find({
                    _id: NewConvo._id
                }).remove(function (err) {
                    if (err) return console.log(err);
                    res.send('User could not be found.')
                });
            }
        });
    });
});

/*app.post('/api/sendImage', function (req, res) {
    var url = req.body.url;
    imgur.upload(url, function (err, d) {
        console.log(d.data.link); // Log the imgur url
        var data = {
            link: d.data.link
        }
        res.send(data);
    });
});*/

io.sockets.on('connection', function (socket) {
    socket.on('subscribe', function (id) {
        console.log('joining room: ', id);
        socket.join(id);
    });

    socket.on('send message', function (data) {
        console.log('sending room post', data.id);
        io.sockets.in(data.id).emit('conversation private post', {
            message: data.message,
            user: data.user,
            timestamp: Date()
        });
    });
});

app.post('/api/findChat', function (req, res) {
    var id = req.body.id;
    console.log(id);
    Convo.findOne({
        _id: id
    }, function (err, convo) {
        if (err) return console.log(err);
        console.log(convo);
        if (convo) {
            res.send(convo);
        } else {
            res.send('There is an issue');
        }
    });
});

app.post('/api/sendChat', function (req, res) {
    var id = req.body.id;
    var user = req.user.username;
    var message = req.body.message;
    Convo.findOneAndUpdate({
        _id: id
    }, {
        $push: {
            'messages': {
                user: user,
                message: message
            }
        }
    }, {
        safe: true,
        upsert: true
    }, function (err, convo) {
        console.log('adding');
        if (err) return console.log(err);
        if (convo) {
            console.log(convo);
            res.send('Good');
        } else {
            res.send('Issue');
        }
    });
});

app.get('/auth/twitter', passport.authenticate('twitter'));

app.get('/auth/twitter/callback',
    passport.authenticate('twitter', {
        failureRedirect: '/'
    }),
    function (req, res) {
        res.redirect('/');
    });

app.get('/logout', function (req, res) {
    req.logout();
    res.redirect('/');
});



// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


module.exports = app;